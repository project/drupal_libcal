<?php

use Drupal\Core\Entity\EntityInterface;
/**
 * @file
 * Contains libcal.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

function libcal_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'libcal/libcal';
}

/**
 * Implements hook_help().
 */
function libcal_help($route_name, RouteMatchInterface $route_match)
{
    switch ($route_name) {
        // Main module help for the libcal module.
        case 'help.page.libcal':
            $output = '';
            $output .= '<h3>' . t('About') . '</h3>';
            $output .= '<p>' . t('This module is integrate Libcal API and feed to drupal') . '</p>';
            return $output;

        default:
    }
}

/**
 * Implements hook_theme().
 */
function libcal_theme()
{
    return [
        'libcal' => [
            'render element' => 'children',
        ],
    ];
}

/**
 *  Implement hook_cron
 */
function libcal_cron()
{
   startDownloadLibCalEvents();
}

/**
 * Trigger download Events process
 */
function startDownloadLibCalEvents() {
  $config = \Drupal::config('libcal.libcalapiconfig');
  $service = \Drupal::service('libcal.download');

  // Download events
  $calendar_ids = explode(',', $config->get('calendar-id'));
  $limit = $config->get('limit');
  $days = $config->get('days');
  $tags = $config->get('tags');

  $query = "events?limit=$limit&days=$days";

  // Append the tags if present.
  // Libcal API accepts a comma separated string so no need to explode()
  // The -1 is to ease transition from the way this module was previously coded
  if ($tags != "" && $tags != "-1") { $query .= "&tag=$tags"; }

  $events = [];

  foreach ($calendar_ids as $cid) {
    $result = $service->get("$query&cal_id=$cid", $config)->events;
    $events = array_merge($events, $result);
  }

  $service->libcalEventToNode($events);
  // Deal with past events
  $service->updatePastFieldEventNode($config);
  // Deal with events that have been deleted from libcal
  $service->deleteEventNodes($events);
}

/**
 * Replace space with special charcater
 * @param $str
 * @param string $delimiter
 * @return string
 */
function createSlug($str, $delimiter = '-')
{
    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    return $slug;
}

/**
 * Generate Path Alias for Event node only
 * @param $node
 */
function generateEventAlias($node)
{
    $tag = "/events/" . createSlug($node->title->value);
    if (!\Drupal::service('path_alias.repository')->lookupByAlias($tag, 'en')) {
        //$path = \Drupal::service('path_alias.repository')->save("/node/" . $node->id(), $tag, "en");
        $path_alias = \Drupal::entityTypeManager()->getStorage('path_alias')->create([
            'path' => "/node/" . $node->id(),
            'alias' => $tag,
            'langcode' => "en",
        ]);
        $path_alias->save();

    }
}

/**
 * Implements hook_insert().
 */
function libcal_entity_insert(EntityInterface $node)
{
    // Set the URL alias
    //if (get_class($node) == 'Drupal\node\Entity\Node') {
    if ($node->getEntityType()->id() == 'node' && in_array($node->getType(), ['event'])) {
        generateEventAlias($node);

        // trigger email sending action.
        $action = \Drupal::entityTypeManager()
          ->getStorage('action')
          ->load('send_email');

        if ($action) {
          $action->execute([$node]);
        }
    }
}

function libcal_entity_update(EntityInterface $node)
{
    // Set the URL alias
    //if (get_class($node) == 'Drupal\node\Entity\Node') {
    if ($node->getEntityType()->id() == 'node' && in_array($node->getType(), ['event'])) {
        generateEventAlias($node);
    }
}

/**
 * Debug function: display any variable to error log
 *
 * @param $thing
 */
if (!function_exists('logging')) {
    function print_log($thing)
    {
        error_log(print_r($thing, true), 0);
    }

}
/**
 * Debug function: display any variable to current webpage
 * @param $thing
 */
if (!function_exists('logging')) {
    function logging($thing)
    {
        echo "<pre>";
        print_r($thing);
        echo "</pre>";
    }
}
